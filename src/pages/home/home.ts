import { Component } from '@angular/core';
import { IonicPage,NavController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }


  newbusiness(){
     this.navCtrl.push('NewbusinessPage');
  }

  newloan(){
     this.navCtrl.push('NewloanPage');
  }

  profile(){
     this.navCtrl.push('ProfilePage');
  }

  report(){
     this.navCtrl.push('ReportPage');
  }

  newbranch(){
     this.navCtrl.push('NewbranchPage');
  }

  collection(){
     this.navCtrl.push('CollectionPage');
  }

  newline(){
     this.navCtrl.push('NewlinePage');
  }

  settings(){
     this.navCtrl.push('SettingsPage');
  }

  myloan(){
     this.navCtrl.push('MyloanPage');
  }

  expense(){
     this.navCtrl.push('ExpensePage');
  }

  access(){
     this.navCtrl.push('AccessPage');
  }

  marksalary(){
     this.navCtrl.push('MarksalaryPage');
  }

  chart(){
     this.navCtrl.push('ChartPage');
  }

  track(){
     this.navCtrl.push('TrackerPage');
  }

  topup(){
     this.navCtrl.push('TopupPage');
  }


}
